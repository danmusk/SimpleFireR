﻿using System;
using Hangfire;
using Serilog.Context;
using SimpleInjector;

namespace SimpleFireR.Web
{
    public class SimpleInjectorJobActivator : JobActivator
    {
        private readonly Container _container;
        private readonly Lifestyle _lifestyle;

        public SimpleInjectorJobActivator(Container container)
        {
            if (container == null)
            {
                throw new ArgumentNullException("container");
            }

            _container = container;
        }

        public SimpleInjectorJobActivator(Container container, Lifestyle lifestyle)
        {
            if (container == null)
            {
                throw new ArgumentNullException("container");
            }

            if (lifestyle == null)
            {
                throw new ArgumentNullException("lifestyle");
            }

            _container = container;
            _lifestyle = lifestyle;
        }

        public override object ActivateJob(Type jobType)
        {
            return _container.GetInstance(jobType);
        }

        public override JobActivatorScope BeginScope(JobActivatorContext context)
        {
            LogContext.PushProperty("RequestGuid", Guid.NewGuid());
            LogContext.PushProperty("HangfireJobId", context.BackgroundJob.Id);
            if (_lifestyle == null || _lifestyle != Lifestyle.Scoped)
            {
                return new SimpleInjectorScope(_container, SimpleInjector.Lifestyles.AsyncScopedLifestyle.BeginScope(_container));
            }
            return new SimpleInjectorScope(_container, new SimpleInjector.Lifestyles.AsyncScopedLifestyle().GetCurrentScope(_container));
        }
    }

    internal class SimpleInjectorScope : JobActivatorScope
    {
        private readonly Container _container;
        private readonly Scope _scope;

        public SimpleInjectorScope(Container container, Scope scope)
        {
            _container = container;
            _scope = scope;
        }

        public override object Resolve(Type type)
        {
            return _container.GetInstance(type);
        }

        public override void DisposeScope()
        {
            if (_scope != null)
            {
                _scope.Dispose();
            }
        }
    }
}