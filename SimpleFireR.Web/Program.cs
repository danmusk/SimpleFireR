﻿using System;
using System.IO;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Events;

namespace SimpleFireR.Web
{
    public class Program
    {
        public static int Main(string[] args)
        {
            IConfigurationBuilder configurationBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();

            // Configure logger
            var loggerConfiguration = new LoggerConfiguration()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                .MinimumLevel.Override("System", LogEventLevel.Warning)
                .MinimumLevel.Override("Hangfire", LogEventLevel.Information)
                .MinimumLevel.Verbose()
                .WriteTo.Seq("http://localhost:5341")
                .Enrich.FromLogContext();


            //IConfiguration configuration = configurationBuilder.Build();
            //if (Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == EnvironmentName.Development)
            //{
            //    loggerConfiguration.WriteTo.Seq("http://localhost:5341");
            //}
            //else
            //{
            //    // Configure logger for production
            //}

            Log.Logger = loggerConfiguration.CreateLogger();

            try
            {
                Log.Information("Start WebHost");
                BuildWebHost(args).Run();
                Log.Information("End WebHost");
                return 0;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly");
                return 1;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseSerilog()
                .Build();
    }
}
