using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Serilog.Context;

namespace SimpleFireR.Web.Middleware
{
    public class EnrichSerilogWithRequestGuidMiddleware
    {
        private readonly RequestDelegate _next;
        public EnrichSerilogWithRequestGuidMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            LogContext.PushProperty("RequestGuid", Guid.NewGuid());
            await _next.Invoke(context);
        }
    }
}