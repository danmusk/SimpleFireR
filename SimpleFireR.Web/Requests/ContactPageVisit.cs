﻿using System;
using MediatR;

namespace SimpleFireR.Web.Requests
{
    public class ContactPageVisit : IRequest
    {
        public DateTimeOffset DateTimeVisited { get; set; }
    }
}