using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Serilog;

namespace SimpleFireR.Web.Requests.Generic
{
    public class PipelineLogger<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
    {
        private readonly ILogger _logger = Log.ForContext<PipelineLogger<TRequest, TResponse>>();

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            _logger.Verbose($"Start pipeline for {request.GetType().Name}");

            var response = await next();

            _logger.Verbose($"End pipeline for {request.GetType().Name} with response {response.GetType().Name}");

            return response;
        }
    }
}
