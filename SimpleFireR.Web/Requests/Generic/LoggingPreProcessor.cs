using System.Threading;
using System.Threading.Tasks;
using MediatR.Pipeline;
using Serilog;

namespace SimpleFireR.Web.Requests.Generic
{
    public class LoggingPreProcessor<TRequest> : IRequestPreProcessor<TRequest>
    {
        private readonly ILogger _logger = Log.ForContext<LoggingPreProcessor<TRequest>>();

        public Task Process(TRequest request, CancellationToken cancellationToken)
        {
            _logger.Verbose($"PreProcessor for {request.GetType().Name}");

            return Task.CompletedTask;
        }
    }
}