using System.Threading.Tasks;
using MediatR.Pipeline;
using Serilog;

namespace SimpleFireR.Web.Requests.Generic
{
    public class LoggingPostProcessor<TRequest, TResponse> : IRequestPostProcessor<TRequest, TResponse>
    {
        private readonly ILogger _logger = Log.ForContext<LoggingPostProcessor<TRequest, TResponse>>();

        public Task Process(TRequest request, TResponse response)
        {
            _logger.Verbose($"PostProcessor for {request.GetType().Name} with response {response.GetType().Name}");

            return Task.CompletedTask;
        }
    }
}