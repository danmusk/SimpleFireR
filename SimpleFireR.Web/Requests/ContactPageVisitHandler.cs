﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Hangfire;
using MediatR;
using Serilog;
using SimpleFireR.Web.Services;

namespace SimpleFireR.Web.Requests
{
    public class ContactPageVisitHandler : IRequestHandler<ContactPageVisit>
    {
        private readonly ICurrentUser _currentUser;
        private readonly IHardWorkingWorker _hardWorkingWorker;

        private readonly ILogger _logger = Log.ForContext<ContactPageVisitHandler>();

        public ContactPageVisitHandler(ICurrentUser currentUser, IHardWorkingWorker hardWorkingWorker)
        {
            _currentUser = currentUser;
            _hardWorkingWorker = hardWorkingWorker;
        }

        public Task Handle(ContactPageVisit request, CancellationToken cancellationToken)
        {
            _logger.Information("Dispatching to Hangfire");
            BackgroundJob.Enqueue(() => _hardWorkingWorker.DoStuff(DateTimeOffset.UtcNow, _currentUser.Name));

            return Task.CompletedTask;
        }
    }
}