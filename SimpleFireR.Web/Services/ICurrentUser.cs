﻿namespace SimpleFireR.Web.Services
{
    public interface ICurrentUser
    {
        string Name { get; }
        string Email { get; }
    }
}