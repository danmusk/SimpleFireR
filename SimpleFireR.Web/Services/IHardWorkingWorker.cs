﻿using System;

namespace SimpleFireR.Web.Services
{
    public interface IHardWorkingWorker
    {
        void DoStuff(DateTimeOffset date);
    }
}