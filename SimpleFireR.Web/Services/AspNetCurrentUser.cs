﻿using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;

namespace SimpleFireR.Web.Services
{
    public class AspNetCurrentUser : ICurrentUser
    {
        private readonly IHttpContextAccessor _accessor;

        public AspNetCurrentUser(IHttpContextAccessor accessor)
        {
            _accessor = accessor;
        }

        public string Name
        {
            get
            {
                if (_accessor.HttpContext == null)
                    return "System (Hangfire)";
                if (_accessor.HttpContext.User.Identity.IsAuthenticated)
                    return _accessor.HttpContext.User.Claims.First(x => x.Type == ClaimTypes.NameIdentifier).Value;
                return "Anonymous";
            }
        }

        public string Email
        {
            get
            {
                if (_accessor.HttpContext == null)
                    return string.Empty;
                if (_accessor.HttpContext.User.Identity.IsAuthenticated)
                    return _accessor.HttpContext.User.Claims.First(x => x.Type == ClaimTypes.Email).Value;
                return string.Empty;
            }
        }
    }
}