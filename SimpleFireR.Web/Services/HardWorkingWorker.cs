﻿using System;
using System.Threading;

namespace SimpleFireR.Web.Services
{
    public class HardWorkingWorker : IHardWorkingWorker
    {
        private readonly ICurrentUser _currentUser;

        public HardWorkingWorker(ICurrentUser currentUser)
        {
            _currentUser = currentUser;
        }

        public void DoStuff(DateTimeOffset date)
        {
            Console.WriteLine("Start process");

            var currentUserName = _currentUser.Name;
            Console.WriteLine($"CurrentUser: {currentUserName}");

            Console.WriteLine("This is going to take some time...");
            Console.WriteLine($"Someone visited the Contactpage on {date}");

            Thread.Sleep(5000);
            Console.WriteLine("End process");
        }
    }
}