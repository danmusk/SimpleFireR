﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Hangfire;
using Hangfire.SqlServer;
using MediatR;
using MediatR.Pipeline;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using SimpleFireR.Web.Data;
using SimpleFireR.Web.Middleware;
using SimpleFireR.Web.Models;
using SimpleFireR.Web.Requests;
using SimpleFireR.Web.Requests.Generic;
using SimpleFireR.Web.Services;
using SimpleInjector;
using SimpleInjector.Integration.AspNetCore.Mvc;
using SimpleInjector.Lifestyles;

namespace SimpleFireR.Web
{
    public class Startup
    {
        private Container container = new Container();
        private ILogger logger = Log.ForContext<Startup>();

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            // Configure Hangfire
            var simpleInjectorJobActivator = new SimpleInjectorJobActivator(container);
            var hangfireDatabaseOptions = new SqlServerStorageOptions
            {
                PrepareSchemaIfNecessary = true                
            };
            services.AddHangfire(x => x.UseSqlServerStorage(Configuration.GetConnectionString("HangfireConnection"), hangfireDatabaseOptions)
                    .UseActivator(simpleInjectorJobActivator));

            services.AddMvc();

            IntegrateSimpleInjector(services);
        }

        private void IntegrateSimpleInjector(IServiceCollection services)
        {
            container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddSingleton<IControllerActivator>(new SimpleInjectorControllerActivator(container));
            services.AddSingleton<IViewComponentActivator>(new SimpleInjectorViewComponentActivator(container));

            services.EnableSimpleInjectorCrossWiring(container);
            services.UseSimpleInjectorAspNetRequestScoping(container);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            // Add a unique guid for each request
            app.UseMiddleware<EnrichSerilogWithRequestGuidMiddleware>();
            
            InitializeContainer(app);

            if (env.IsDevelopment())
            {
                //app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseHangfireServer();
            app.UseHangfireDashboard();

            app.UseMiddleware<SerilogMiddleware>();

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }

        private void InitializeContainer(IApplicationBuilder app)
        {
            logger.Information("Start InitializeContainer");

            // Add application presentation components:
            container.RegisterMvcControllers(app);
            container.RegisterMvcViewComponents(app);

            // Add application services. For instance:
            container.Register<IEmailSender, EmailSender>(Lifestyle.Scoped);

            container.Register<ICurrentUser, AspNetCurrentUser>(Lifestyle.Scoped);
            //container.Register<ICurrentUser, HangFireUser>(Lifestyle.Scoped);
           
            container.Register<IHardWorkingWorker, HardWorkingWorker>(Lifestyle.Scoped);

            // Start MediatR config
            var assemblies = GetAssemblies().ToArray();

            container.Register<IMediator, Mediator>(Lifestyle.Scoped);
            container.Register(typeof(IRequestHandler<,>), assemblies, Lifestyle.Scoped);
            container.Register(typeof(IRequestHandler<>), assemblies, Lifestyle.Scoped);

            // we have to do this because by default, generic type definitions (such as the Constrained Notification Handler) won't be registered
            var notificationHandlerTypes = container.GetTypesToRegister(typeof(INotificationHandler<>), assemblies, new TypesToRegisterOptions
            {
                IncludeGenericTypeDefinitions = true,
                IncludeComposites = false,
            });
            container.RegisterCollection(typeof(INotificationHandler<>), notificationHandlerTypes);


            //Pipeline
            container.RegisterCollection(typeof(IPipelineBehavior<,>), new[]
            {
                typeof(PipelineLogger<,>),
                typeof(RequestPreProcessorBehavior<,>),
                typeof(RequestPostProcessorBehavior<,>),
            });

            container.RegisterCollection(typeof(IRequestPreProcessor<>), new[] { typeof(LoggingPreProcessor<>) });
            container.RegisterCollection(typeof(IRequestPostProcessor<,>), new[] { typeof(LoggingPostProcessor<,>) });

            container.RegisterInstance(new SingleInstanceFactory(container.GetInstance));
            container.RegisterInstance(new MultiInstanceFactory(container.GetAllInstances));
            // End MediatR config


            // Allow Simple Injector to resolve services from ASP.NET Core.
            container.AutoCrossWireAspNetComponents(app);

            logger.Information("End InitializeContainer");
        }

        private static IEnumerable<Assembly> GetAssemblies()
        {
            yield return typeof(IMediator).GetTypeInfo().Assembly;
            yield return typeof(ContactPageVisit).GetTypeInfo().Assembly;
        }
    }
}
