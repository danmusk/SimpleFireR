AspNet Core 2.0
- Simple Injector
- HangFire
- MediatR
- FluentValidation
- Serilog

Tutorials followed:

Serilog
- https://blog.getseq.net/smart-logging-middleware-for-asp-net-core/
- https://github.com/serilog/serilog-aspnetcore

MediatR
- https://codeopinion.com/mediatr-behaviors/